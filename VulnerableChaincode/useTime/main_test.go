package main

import (

	// "filepath"
	// "flag"
	"fmt"

	"github.com/hyperledger/fabric-chaincode-go/shim"

	// "github.com/hyperledger/fabric-chaincode-go/shimtest"
	"gitee.com/yemingzhi/goErrorPaper/shimtest"
	// "log"

	// "os"
	"sync"
	"testing"
	// "gitee.com/yemingzhi/goErrorPaper/fabric-chaincode-go/shim"
	// "gitee.com/yemingzhi/goErrorPaper/fabric-chaincode-go/shimtest"
)

func TestMain(t *testing.T) {
	conChannum := 3
	waitRandTime := int64(5)
	localPath := "/home/hello/go/src/openChaincode/VulnerableChaincode/useTime"
	shimtest.Init(conChannum, waitRandTime, localPath)

	var wg sync.WaitGroup
	for i := 0; i < shimtest.ConChannum; i++ {
		wg.Add(1)
		go testChaincode(&wg, i)
	}
	wg.Wait()
	fmt.Println("PASS the Test")
}

func testChaincode(wg *sync.WaitGroup, conChanNo int) {
	// fmt.Println("enter getArgsResponse")
	cc := new(SimpleAsset)
	stub := shimtest.NewMockStub("sacc", cc, conChanNo)

	args := [][]byte{[]byte("a"), []byte("10")}
	// fmt.Println("MockInit start")
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("chaincode init err")
		// t.FailNow()
	}

	args = [][]byte{[]byte("addInfo"), []byte("110"), []byte("bbb")}
	res = stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("chaincode invoke addInfo err")
	}

	// args = [][]byte{[]byte("addInfo"), []byte("110"), []byte("ccc")}
	// res = stub.MockInvoke("1", args)
	// if res.Status != shim.OK {
	// 	fmt.Println("chaincode invoke addInfo err")
	// }

	// args = [][]byte{[]byte("addInfo"), []byte("330"), []byte("ddd")}
	// res = stub.MockInvoke("1", args)
	// if res.Status != shim.OK {
	// 	fmt.Println("chaincode invoke addInfo err")
	// }

	// args = [][]byte{[]byte("getInfo"), []byte("110")}
	// res = stub.MockInvoke("1", args)
	// if res.Status != shim.OK {
	// 	fmt.Println("chaincode invoke getInfo err")
	// }

	wg.Done()
}
