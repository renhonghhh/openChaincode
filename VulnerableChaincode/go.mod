module VulnerableChaincode

go 1.17

require (
	gitee.com/yemingzhi/goErrorPaper v0.0.0-20220322135758-1ec2f19ff918
	github.com/hyperledger/fabric-chaincode-go v0.0.0-20220131132609-1476cf1d3206
	github.com/hyperledger/fabric-protos-go v0.0.0-20220315113721-7dc293e117f7
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092 // indirect
	golang.org/x/sys v0.0.0-20190710143415-6ec70d6a5542 // indirect
	golang.org/x/text v0.3.0 // indirect
	google.golang.org/genproto v0.0.0-20180831171423-11092d34479b // indirect
	google.golang.org/grpc v1.23.0 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
)
