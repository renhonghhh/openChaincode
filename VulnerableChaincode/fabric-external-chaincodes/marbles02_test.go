/*
Copyright Hitachi America Ltd. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package main

import (
	"fmt"
	"sync"
	"testing"

	"gitee.com/yemingzhi/goErrorPaper/shimtest"
	"github.com/hyperledger/fabric-chaincode-go/shim"
)

func TestMain(t *testing.T) {
	conChannum := 3
	waitRandTime := int64(5)
	localPath := "/home/hello/go/src/openChaincode/VulnerableChaincode/fabric-external-chaincodes"
	shimtest.Init(conChannum, waitRandTime, localPath)

	var wg sync.WaitGroup
	for i := 0; i < shimtest.ConChannum; i++ {
		wg.Add(1)
		go testChaincode(t, &wg, i)
	}
	wg.Wait()
	fmt.Println("PASS the Test")
}

func testChaincode(t *testing.T, wg *sync.WaitGroup, conChanNo int) {
	// fmt.Println("enter getArgsResponse")
	cc := new(SimpleChaincode)
	stub := shimtest.NewMockStub("sacc", cc, conChanNo)
	// stub.MockInit("1", args)
	args := [][]byte{[]byte("asdf")}
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("Init failed", string(res.Message))
		t.FailNow()
	}

	args = [][]byte{[]byte("initMarble"), []byte("marble1"), []byte("blue"), []byte("35"), []byte("bob")}
	stub.MockInvoke("1110", args)
	wg.Done()
}
