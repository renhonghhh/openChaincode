package main

import (
	// "crypto/rand"
	"fmt"
	// "math/big"
	"sync"
	"testing"

	// "time"
	"errors"
)

var OneOfStagy string = "OneOfStagy"
var N1_3ofStragy string = "N1_3ofStragy"
var N2_3ofStragy string = "N2_3ofStragy"
var AllStragy string = "AllStragy"

func CheckStrage(strategyMap map[string]int, content string, totalN int) (resStragy []string, err error) {

	if len(strategyMap) == 0 {
		err = errors.New("strateMap is empty")
		return
	} else if len(strategyMap) == 1 {
		resStragy = append(resStragy, []string{OneOfStagy, N1_3ofStragy, N2_3ofStragy, AllStragy}...)
		return
	} else if len(strategyMap) > 1 {
		ss := strategyMap[content]
		if ss > 2*totalN/3 {
			resStragy = append(resStragy, []string{OneOfStagy, N1_3ofStragy, N2_3ofStragy}...)
			return
		} else if ss > totalN/3 {
			resStragy = append(resStragy, []string{OneOfStagy, N1_3ofStragy}...)
			return
		} else {
			resStragy = append(resStragy, []string{OneOfStagy}...)
			return
		}
	}

	return
}

func Test_Main(t *testing.T) {
	fmt.Println("start testing")
	t.Run("test_Read_From_Channel", test_Read_From_Channel)

}

func read_Channel(strChan chan string, wg *sync.WaitGroup) {
	strChan <- "hello"
	wg.Done()
}

func test_Read_From_Channel(t *testing.T) {
	strChan := make(chan string, 10)
	var wg sync.WaitGroup

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go read_Channel(strChan, &wg)
	}
	wg.Wait()

	var strategyMap = make(map[string]int, 10)

	for len(strChan) > 0 {
		str := <-strChan
		strategyMap[str] += 1
		fmt.Println("strategyMap[str]", strategyMap[str])
	}

	stragy, _ := CheckStrage(strategyMap, "hello", 10)
	for _, v := range stragy {
		fmt.Println("stragy is", v)
	}

}
