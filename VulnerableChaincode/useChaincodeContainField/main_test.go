package main

import (
	"crypto/rand"
	"errors"
	"fmt"
	"math/big"
	"sync"
	"testing"
	"time"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-chaincode-go/shimtest"
	// "github.com/hyperledger/fabric/scripts/fabric-samples/chaincode/openChaincode/VulnerableChaincode/strategy"
)

var OneOfStagy string = "OneOfStagy"
var N1_3ofStragy string = "N1_3ofStragy"
var N2_3ofStragy string = "N2_3ofStragy"
var AllStragy string = "AllStragy"

func CheckStrage(strategyMap map[string]int, content string, totalN int) (resStragy []string, err error) {

	if len(strategyMap) == 0 {
		err = errors.New("strateMap is empty")
		return
	} else if len(strategyMap) == 1 {
		resStragy = append(resStragy, []string{OneOfStagy, N1_3ofStragy, N2_3ofStragy, AllStragy}...)
		return
	} else if len(strategyMap) > 1 {
		ss := strategyMap[content]
		if ss > 2*totalN/3 {
			resStragy = append(resStragy, []string{OneOfStagy, N1_3ofStragy, N2_3ofStragy}...)
			return
		} else if ss > totalN/3 {
			resStragy = append(resStragy, []string{OneOfStagy, N1_3ofStragy}...)
			return
		} else {
			resStragy = append(resStragy, []string{OneOfStagy}...)
			return
		}
	}

	return
}

func Test_add_get_Info(t *testing.T) {
	// cc := new(SimpleAsset)
	// stub := shimtest.NewMockStub("sacc", cc)
	// args := [][]byte{[]byte("a"), []byte("10")}
	// res := stub.MockInit("1", args)
	// if res.Status != shim.OK {
	// 	fmt.Println("chaincode init err")
	// 	t.FailNow()
	// }

	channelNum := 3

	resStringChan := make(chan string, channelNum)

	var wg sync.WaitGroup
	for i := 0; i < channelNum; i++ {
		wg.Add(1)
		go getArgsResponse(resStringChan, &wg)
	}
	wg.Wait()

	//create a set to check consistent
	set := make(map[string]int)

	for len(resStringChan) > 0 {
		temp := <-resStringChan
		// fmt.Println("temp", temp)
		set[temp] += 1
	}
	// fmt.Println("len(set)", len(set))
	stragy, _ := CheckStrage(set, "", 3)
	fmt.Println(stragy)

	if len(set) > 1 {
		fmt.Println("response is different, consistent error")
		t.FailNow()
	}
}

func getArgsResponse(resStrings chan string, wg *sync.WaitGroup) {

	randNum, _ := rand.Int(rand.Reader, big.NewInt(5))
	fmt.Println("randNum", randNum)

	cc := new(SimpleAsset)
	stub := shimtest.NewMockStub("sacc"+randNum.String(), cc)
	fmt.Println("stub", stub)
	args := [][]byte{[]byte("a"), []byte("10")}
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("chaincode init err")
		// t.FailNow()
	}

	//simulate network delay

	sleepTime := time.Second * time.Duration(randNum.Int64())
	time.Sleep(sleepTime)

	args = [][]byte{[]byte("addInfo"), []byte("110"), []byte("bbb")}
	res = stub.MockInvoke("1", args)

	if res.Status != shim.OK {
		fmt.Println("chaincode invoke addInfo err")
	}

	if randNum.Int64()%3 == 0 {
		args = [][]byte{[]byte("updateInfo"), []byte("110"), []byte("bbb")}
		res := stub.MockInvoke("1", args)
		if res.Status != shim.OK {
			fmt.Println("chaincode invoke updateInfo err")
		}
	}

	args = [][]byte{[]byte("getInfo"), []byte("110")}
	res = stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("chaincode invoke getInfo err")
	}

	resStrings <- string(res.Payload)
	fmt.Println("res.Payload", string(res.Payload))
	wg.Done()
}
