package main

import (

	// "filepath"
	// "flag"
	"fmt"

	"github.com/hyperledger/fabric-chaincode-go/shim"

	// "github.com/hyperledger/fabric-chaincode-go/shimtest"
	"gitee.com/yemingzhi/goErrorPaper/shimtest"
	// "log"

	// "os"
	"sync"
	"testing"
	// "gitee.com/yemingzhi/goErrorPaper/fabric-chaincode-go/shim"
	// "gitee.com/yemingzhi/goErrorPaper/fabric-chaincode-go/shimtest"
)

func TestMain(t *testing.T) {
	//可选择设定的参数1，模拟背书节点的个数
	conChannum := 3
	//可选择设定的参数2，模拟背书节点经历网络时间延迟的时间范围
	waitRandTime := int64(5)
	//要修改的第1个地方，测试合约的绝对路径。
	localPath := `C:/Users/Administrator/go/src/openChaincode/VulnerableChaincode/useRand`
	shimtest.Init(conChannum, waitRandTime, localPath)

	var wg sync.WaitGroup
	for i := 0; i < shimtest.ConChannum; i++ {
		wg.Add(1)
		go testChaincode(&wg, i)
	}
	wg.Wait()
	// fmt.Println("PASS the Test")
}

func testChaincode(wg *sync.WaitGroup, conChanNo int) {
	//要修改的第2个地方，被测试链码的结构体名称。
	cc := new(SimpleAsset)
	stub := shimtest.NewMockStub("sacc", cc, conChanNo)

	//要修改的第3个地方，被测试函数的输入参数
	args := [][]byte{[]byte("a"), []byte("10")}
	// fmt.Println("MockInit start")
	res := stub.MockInit("1", args)
	if res.Status != shim.OK {
		fmt.Println("chaincode init err")
		// t.FailNow()
	}

	args = [][]byte{[]byte("addInfo"), []byte("110"), []byte("bbb")}
	res = stub.MockInvoke("1", args)
	if res.Status != shim.OK {
		fmt.Println("chaincode invoke addInfo err")
	}

	wg.Done()
}
